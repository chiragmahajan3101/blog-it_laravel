<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidateCategoryEdit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (is_object($request->category) && !(auth()->user()->isAdmin())) {
            if ($request->category->approved) {
                abort(404, 'Cannot misinterpret Approved One! Only Admin can');
            }
        }
        return $next($request);
    }
}
