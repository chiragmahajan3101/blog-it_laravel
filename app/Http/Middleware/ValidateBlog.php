<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidateBlog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (is_object($request->post)) {
            if (!$request->post->approved) {
                abort(404);
            }
        }
        return $next($request);
    }
}
