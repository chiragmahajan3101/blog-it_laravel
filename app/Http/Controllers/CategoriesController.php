<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['validateCategoryEdit'])->only('edit', 'update', 'destroy');
    }
    public function index()
    {
        $categories = Category::latest('updated_at')->paginate(3);
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        Category::create([
            'name' => $request->name
        ]);
        session()->flash('success', 'Category created successfully');
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.edit', compact(['category']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->name = $request->name;
        // $category->update(['name' => $request->name]);
        $category->save();

        session()->flash('success', 'Category Updated Successfully!');
        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->posts->count() > 0) {
            session()->flash('error', 'This Category cannot be deleted!');
            return redirect(route('categories.index'));
        }
        $category->delete();
        session()->flash('success', 'Category deleted sucessfully!');
        return redirect(route('categories.index'));
    }

    public function status(Category $category)
    {
        if ($category->approved) {
            if ($category->posts()->count()) {
                session()->flash('error', 'Category Status cannot be changed there are posts present!');
                return redirect(route('categories.index'));
            }
        }
        $category->approved = !($category->approved);
        $category->save();
        session()->flash('success', 'Category Status is changed successfully!');
        return redirect(route('categories.index'));
    }
}
