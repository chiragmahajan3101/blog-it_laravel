<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $posts = Post::search()->status()->latest('published_at')->published()->simplePaginate(3);
        $tags = Tag::status()->get();
        $categories = Category::status()->get();
        return view('blogs.index', compact(['posts', 'tags', 'categories']));
    }

    public function show(Post $post)
    {

        if (auth()->check() && (auth()->id() != $post->user_id)) {
            $post->increment('views_count');
            if (!($post->usersLiked()->where(['user_id' => auth()->id()])->exists())) {
                $post->usersLiked()->attach(auth()->id());
            }
        }

        $tags = Tag::status()->get();
        $categories = Category::status()->get();
        return view('blogs.post', compact(['categories', 'post', 'tags']));
    }

    public function category(Category $category)
    {
        $posts = $category->posts()->search()->status()->published()->latest('published_at')->simplePaginate(3);
        $tags = Tag::status()->get();
        $categories = Category::status()->get();
        return view('blogs.index', compact(['categories', 'posts', 'tags']));
    }

    public function tag(Tag $tag)
    {
        $posts = $tag->posts()->search()->status()->published()->latest('published_at')->simplePaginate(3);
        $tags = Tag::status()->get();
        $categories = Category::status()->get();
        return view('blogs.index', compact(['categories', 'posts', 'tags']));
    }
}
