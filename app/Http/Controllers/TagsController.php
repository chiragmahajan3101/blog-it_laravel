<?php

namespace App\Http\Controllers;


use App\Http\Requests\Tags\CreateTagRequest;
use App\Http\Requests\Tags\UpdateTagRequest;
use App\Models\Tag;


class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['validateTagEdit'])->only('edit', 'update', 'destroy');
    }

    public function index()
    {
        $tags = Tag::latest('updated_at')->paginate(10);
        return view('tags.index', compact(['tags']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request)
    {
        Tag::create(['name' => $request->name]);

        session()->flash('success', 'Tag created successfully!');

        return redirect(route('tags.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('tags.edit', compact(['tag']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tag->name = $request->name;
        // $category->update(['name' => $request->name]);
        $tag->save();
        session()->flash('success', 'Tag updated successfully!');

        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if ($tag->posts->count() > 0) {
            session()->flash('error', 'This Tag cannot be deleted!');
            return redirect(route('tags.index'));
        }
        $tag->delete();

        session()->flash('success', 'Tag deleted successfully!');

        return redirect(route('tags.index'));
    }

    public function status(Tag $tag)
    {
        if ($tag->approved) {
            if ($tag->posts()->count()) {
                session()->flash('error', 'Tag Status cannot be changed there are posts present!');
                return redirect(route('tags.index'));
            }
        }
        $tag->approved = !($tag->approved);
        $tag->save();
        session()->flash('success', 'Tag Status is changed successfully!');
        return redirect(route('tags.index'));
    }
}
