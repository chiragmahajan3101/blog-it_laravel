<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Notifications\PostLiked;
use Illuminate\Http\Request;

class LikesController extends Controller
{
    public function likePost(Post $post, int $like)
    {
        if (auth()->user()->hasReactionForPost($post)) {
            if (($like == 1 && !auth()->user()->hasLikeForPost($post)) || ($like == -1 && !auth()->user()->hasDislikeForPost($post))) {
                $post->updateLike($like);
            }
        } else {
            $post->like($like);
        }
        if (($post->likes_count % 100) == 0) {
            $post->author->notify(new PostLiked($post));
        }
        return redirect()->back();
    }
}
