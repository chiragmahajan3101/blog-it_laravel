<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    const ADMIN = 'admin';
    const AUTHOR = 'author';
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasLikeForPost(Post $post)
    {
        return auth()->user()->likedPost()->where(['post_id' => $post->id, 'like' => 1])->exists();
    }

    public function hasDislikeForPost(Post $post)
    {
        return auth()->user()->likedPost()->where(['post_id' => $post->id, 'like' => -1])->exists();
    }

    public function hasReactionForPost(Post $post)
    {
        return $this->hasLikeForPost($post) || $this->hasDislikeForPost($post);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function likedPost()
    {
        return $this->belongsToMany(Post::class)->withTimestamps();
    }
    public function isAdmin(): bool
    {
        return $this->role === self::ADMIN;
    }

    public function getGravatarImageAttribute(): string
    {
        return Gravatar::src($this->email, 80);
    }
}
