<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Chirag Mahajan',
            'email' => 'chiragmahajan3101@gmail.com',
            'password' => Hash::make('chirag3101'),
            'role' => 'admin',
        ]);

        User::create([
            'name' => 'Prachi Mahajan',
            'email' => 'prachimahajan1501@gmail.com',
            'password' => Hash::make('prachi1501')
        ]);
    }
}
