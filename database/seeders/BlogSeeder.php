<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use Carbon\Carbon;
use Faker\Factory;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name' => 'News', 'approved' => 1]);
        $categoryDesign = Category::create(['name' => 'Design', 'approved' => 1]);
        $categoryTechnology = Category::create(['name' => 'Technology', 'approved' => 1]);
        $categoryEngineering = Category::create(['name' => 'Engineering', 'approved' => 1]);

        $tagCustomers = Tag::create(['name' => 'customers', 'approved' => 1]);
        $tagDesign = Tag::create(['name' => 'design', 'approved' => 1]);
        $tagLaravel = Tag::create(['name' => 'laravel', 'approved' => 1]);
        $tagCoding = Tag::create(['name' => 'coding', 'approved' => 1]);

        $post1 = Post::create([
            'title' => 'We relocated our office to HOME!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraph(rand(3, 7), true),
            'image' => 'images/posts/1.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved' => 1
        ]);

        $post2 = Post::create([
            'title' => 'Lets make something creative!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraph(rand(3, 7), true),
            'image' => 'images/posts/2.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved' => 1
        ]);

        $post3 = Post::create([
            'title' => 'Covid vaccination drive is on!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraph(rand(3, 7), true),
            'image' => 'images/posts/3.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved' => 1
        ]);

        $post4 = Post::create([
            'title' => 'Why engineering is coolest!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraph(rand(3, 7), true),
            'image' => 'images/posts/4.jpg',
            'category_id' => $categoryEngineering->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved' => 1
        ]);

        $post5 = Post::create([
            'title' => 'Updates in Java!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraph(rand(3, 7), true),
            'image' => 'images/posts/2.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 1,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved' => 1
        ]);

        $post1->tags()->attach([$tagCustomers->id, $tagDesign->id]);
        $post2->tags()->attach([$tagDesign->id]);
        $post3->tags()->attach([$tagCustomers->id]);
        $post4->tags()->attach([$tagCoding->id]);
        $post5->tags()->attach([$tagCoding->id, $tagLaravel->id, $tagDesign->id]);
    }
}
