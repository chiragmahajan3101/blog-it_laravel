@extends('layouts.admin-panel.app')

@section('content')
<div class="d-flex justify-content-end mb-3">
    <a href="{{route('categories.create')}}" class="btn btn-outline-primary">Add Category</a>
</div>
<div class="card">
    <div class="card-header"><h2>Categories</h2></div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    @if(!(auth()->user()->isAdmin()))
                    <th>Status</th>
                    @endif
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        @if(!(auth()->user()->isAdmin()))
                        <td>
                            <span class="btn btn-sm {{$category->approved ? 'btn-outline-success' : 'btn-outline-danger'}}">
                                {{$category->approved ? 'Approved' : 'Disapproved'}}
                            </span>
                        </td>
                        @endif
                        <td>
                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-sm btn-primary">
                                Edit
                            </a>
                            <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $category->id }})" data-toggle="modal" data-target="#deleteModal">
                                Delete
                            </button>
                            @if(auth()->user()->isAdmin())
                            <div class="mt-2">
                                <form action="{{route('categories.status', $category->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-sm {{$category->approved ? 'btn-outline-danger' : 'btn-outline-success'}}">
                                    @if (! $category->approved)
                                        Approve <i class="fa fa-check"></i>
                                    @else
                                        Disapprove <i class="fa fa-times"></i>
                                    @endif
                                    </button>

                                </form>
                            </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="" method="POST" id="deleteCategoryForm">
            @csrf
            @method('DELETE')
        <div class="modal-body">
          Are you sure, you want to delete this category?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-outline-danger">Delete Category</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<div class="mt-5">
    {{ $categories->links('vendor.pagination.bootstrap-4') }}
</div>

@endsection

@section('page-level-scripts')
<script>
    function displayModal(categoryId) {
        var url = "/categories/" + categoryId;
        $("#deleteCategoryForm").attr('action', url);
    }
</script>
@endsection
