@extends('layouts.admin-panel.app')

@section('content')
<div class="d-flex justify-content-end mb-3">
    <a href="{{route('tags.create')}}" class="btn btn-outline-primary">Add Tag</a>
</div>
<div class="card">
    <div class="card-header"><h2>Tags</h2></div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    @if(!(auth()->user()->isAdmin()))
                    <th>Status</th>
                    @endif
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tags as $tag)
                    <tr>
                        <td>{{ $tag->name }}</td>
                        @if(!(auth()->user()->isAdmin()))
                        <td>
                            <span class="btn btn-sm {{$tag->approved ? 'btn-outline-success' : 'btn-outline-danger'}}">
                                {{$tag->approved ? 'Approved' : 'Disapproved'}}
                            </span>
                        </td>
                        @endif
                        <td>
                            <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-sm btn-primary">
                                Edit
                            </a>
                            <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $tag->id }})" data-toggle="modal" data-target="#deleteModal">
                                Delete
                            </button>
                            @if(auth()->user()->isAdmin())
                            <div class="mt-2">
                                <form action="{{route('tags.status', $tag->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-sm {{$tag->approved ? 'btn-outline-danger' : 'btn-outline-success'}}">
                                    @if (! $tag->approved)
                                        Approve <i class="fa fa-check"></i>
                                    @else
                                        Disapprove <i class="fa fa-times"></i>
                                    @endif
                                    </button>
                                </form>
                            </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="" method="POST" id="deleteTagForm">
            @csrf
            @method('DELETE')
        <div class="modal-body">
          Are you sure, you want to delete this tag?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-outline-danger">Delete Tag</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<div class="mt-5">
    {{ $tags->links('vendor.pagination.bootstrap-4') }}
</div>

@endsection

@section('page-level-scripts')
<script>
    function displayModal(tagId) {
        var url = "/tags/" + tagId;
        $("#deleteTagForm").attr('action', url);
    }
</script>
@endsection
